﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace Reseptiappi
{
    class RecipeChecker
    {
        public List<Recipe> Check(List <string> ingredients)
        {
            // Gets the recipes from the xml
            XElement doc = XElement.Load("Recipes.xml");
            IEnumerable<XElement> recipes = doc.Elements();
            
            List<Recipe> matchingRecipes = new List<Recipe>();
            List<string> recipeIngredients = new List<string>();

            foreach (var recipe in recipes)
            {
                // Collects all the ingredients in a recipe and converts them into a string list
                foreach (var ingredient in recipe.Elements("Ingredient"))
                {
                    recipeIngredients.Add(ingredient.Value);
                }

                bool isMatch = false;
                for (int i = 0; i < recipeIngredients.Count; i++)
                {
                    isMatch = isFound(recipeIngredients[i], ingredients);
                    if(!isMatch)
                    {
                        break;
                    }
                }
                if (isMatch)
                {
                    Recipe r = new Recipe(recipe.FirstAttribute.Value, recipeIngredients);
                    matchingRecipes.Add(r);
                }
                
                recipeIngredients.Clear();
            }

            return matchingRecipes;
        }

        bool isFound(string ingredient, List<string> ingredients)
        {
            if(ingredients.Contains(ingredient))
            {
                return true;
            }
            return false;
        }
    }
}
