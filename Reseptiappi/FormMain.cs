﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Reseptiappi
{
    public partial class FormMain : Form
    {
        RecipeChecker check = new RecipeChecker();
        RecipeLoader load = new RecipeLoader();
        List<Recipe> recipes = new List<Recipe>();
        List<string> inputRecipes = new List<string>();
        List<string> cookingSteps = new List<string>();

        public FormMain()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if(!inputRecipes.Contains(textBox1.Text))
            {
                inputRecipes.Add(textBox1.Text);
                listBox1.Items.Add(inputRecipes[inputRecipes.Count - 1]);
                textBox1.Clear();
            }

            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.DataSource = null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            recipes = check.Check(inputRecipes);
            foreach (Recipe recipe in recipes)
            {
                listBox2.Items.Add(recipe.Name);
            }

            if(recipes.Count > 0)
            {
                listBox1.Items.Clear();
                inputRecipes.Clear();
            }

            else
            {
                MessageBox.Show("No recipes available with given ingredients");
            }
            
        }

        private void Remove_Click(object sender, EventArgs e)
        {
            string ingredientName = listBox1.SelectedItem.ToString();
            listBox1.Items.Remove(ingredientName);
            inputRecipes.Remove(ingredientName);
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string recipeName = listBox2.SelectedItem.ToString();
            foreach(Recipe recipe in recipes)
            {
                if(recipe.Name == recipeName)
                {
                    // We can get the datasource because loadinfo returns a string list
                    listBox4.DataSource = load.LoadInfo(recipe);
                    // Loadinfo doesn't work with the ingredients because we don't get string list return
                    List<Ingredient> i = recipe.Ingredients;
                    foreach(Ingredient ingredient in i)
                    {
                        string amount = load.LoadAmount(ingredient);
                        listBox3.Items.Add(ingredient.Name + " (" + amount + ")");
                    } 
                }
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Add_Click(this, new EventArgs());
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
    }
}
