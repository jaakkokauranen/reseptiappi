﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace Reseptiappi
{
    class RecipeLoader
    {
        XElement doc = XElement.Load("Recipes.xml");
        public List<string> LoadInfo(Recipe selectedRecipe)
        {
            IEnumerable<XElement> recipes = doc.Elements();
            List<string> steps = new List<string>();

            foreach(var recipe in recipes)
            {
                if(recipe.FirstAttribute.Value == selectedRecipe.Name)
                {
                    foreach (var step in recipe.Elements("Info"))
                    {
                        steps.Add(step.Value);
                    }
                    break;
                }
            }

            return steps;
        }

        public string LoadAmount(Ingredient selectedIngredient)
        {
            IEnumerable<XElement> recipes = doc.Elements();
            string amount = "";

            foreach (var recipe in recipes)
            {
                foreach (var ingredient in recipe.Elements("Ingredient"))
                {
                    if (ingredient.Value == selectedIngredient.Name)
                    {
                        amount = ingredient.FirstAttribute.Value;
                    }
                }
            }

            return amount;
        }
    }
}
