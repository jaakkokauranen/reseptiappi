﻿namespace Reseptiappi
{
    class Ingredient
    {
        public string Name { get; private set; }

        public Ingredient(string name)
        {
            Name = name;
        }
    }
}
