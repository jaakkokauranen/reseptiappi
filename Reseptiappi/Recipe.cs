﻿using System.Collections.Generic;

namespace Reseptiappi
{
    class Recipe
    {
        public string Name { get; private set; }

        List<Ingredient> ingredients = new List<Ingredient>();
        public List<Ingredient> Ingredients
        {
            get { return ingredients; }
        }

        public Recipe(string name, List<string> ingredientList)
        {
            Name = name;
            foreach(string ingredient in ingredientList)
            {
                Ingredient i = new Ingredient(ingredient);
                ingredients.Add(i);
            }
        }
    }
}
